package com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.persona

import org.springframework.data.jpa.repository.JpaRepository

interface PersonaRepository : JpaRepository<Persona, Long> {
    fun findFirstByFirstNameAndLastName(firstName: String, lastName: String): Persona?
}